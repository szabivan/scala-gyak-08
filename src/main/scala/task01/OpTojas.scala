package task01

/**
 * Egy Húsvéti Tojásnak van
 * - színeinek száma, ami egy Int
 * - mintájának neve, ami egy String
 * - súlya, ami egy Int
 */
case class Tojas(szinekSzama: Int, mintaNeve: String, suly: Int)

object OpTojas extends App {


  /**
   * Írjunk függvényt, ami kap egy Option[Tojas] vektort, és
   * - ha van olyan Some(Tojas), ami a vektorban egyedül van a legtöbb színnel, akkor
   * azt adja vissza egy Option[Tojas]-ban Some-ként,
   * - ha pedig több tojás is van maximális számú színnel, vagy üres a vektor,
   * akkor pedig None-t ad vissza!
   */
  def legTarkabbTojas(optojassz: Vector[Option[Tojas]]): Option[Tojas] = ???
}

package task04

/**
 * Map, Option, Vector gyakorló feladatok, részben Gercsó Márk java gyakorló feladatsorai alapján.
 * (azért is, hogy lássuk a két nyelv közti különbségeket.)
 *
 * Sample megoldásokat hozzá lehet találni a kapcsolódó teszt osztályban: ott ezúttal a teszt úgy zajlik,
 * hogy egy saját implementációm outputjával hasonlítom össze az implementált változatot.
 *
 * A kommenteket érdemes lehet átfutni a feladatkiírásokban is, és a teszt osztályban is,
 * van, ahol eddig még nem látott nyelvi funkciók szerepelnek :)
 */

trait Consumable //megehető dolgok típusa

//NOTE: trait extendelhet traitet!
trait Eloleny extends Consumable{ //az élőlények megehető dolgok
  def canEat(food: Consumable): Boolean //true akkor, ha az adott élőlény eszi az adott ételt
}
//NOTE: a trait abban hasonlít egy Java absztrakt osztályhoz, hogy lehetnek benne definiált metódusok is.
trait Noveny extends Eloleny{ //minden növény egyben élőlény is (tehát consumable is)
  //FELADAT: oldjuk meg, hogy a növények canEat metódusa mindenképp false-t adjon vissza!
  override def canEat(food: Consumable): Boolean = ???
}
//NOTE: a match-beli case x : String minta akkor illeszkedik, ha a matchelt objektum típusa megfelel a String-nek
//(pl leszármazott osztályra is illeszkedne); a case => után az x egy String lesz, amit a matchelt objektum
//"Stringre castolásával" kapnánk.
trait Allat extends Eloleny{
  def isCarnivore: Boolean //visszaadja, hogy az állat (kizárólag) húsevő-e
  def isHerbivore: Boolean //visszaadja, hogy az állat (kizárólag) nővényevő-e
  def isOmnivore: Boolean //visszaadja, hogy az állat mindenevő-e
  //FELADAT: oldjuk meg, hogy az állatok canEat metódusa konzisztens legyen a fenti három metódusukkal, vagyis
  //növényevő állatok pontosan a növényeket, húsevő állatok pontosan az állatokat, mindenevők pedig mindent canEat-eljenek!
  override def canEat(food: Consumable): Boolean = ???
}

object PracticeMOV extends App {
  /**
   * Feladat. Írjunk függvényt, mely "safe" castot végez: a bejövő Eloleny ha egy Allat, akkor visszaadja
   * Some[Allat]-ként, ha pedig nem az, akkor None-t ad vissza!
   */
  def castToAllat(being: Eloleny): Option[Allat] = ???

  /**
   * FELADAT. Írjunk függvényt, mely a bejövő Vector[Eloleny]-ből leválogatja a növényeket!
   * Az eredmény típusa legyen Vector[Noveny]!
   *
   * NOTE: a filter nem változtatja meg a típusát az input vectornak.
   * Egy megoldás lehet az asInstanceOf[T] metódus használata, ami lefelé próbál castolni, de van más út is.
   */
  def collectPlants(beings: Vector[Eloleny]): Vector[Noveny] = ???

  /**
   * FELADAT. Írjunk függvényt, mely egy Vector[Allat]-ból egy három Vector[Allat]-ból álló tuplét ad vissza:
   * az elsőbe kerüljenek a ragadozók, a másodikba a növényevők, a harmadikba a mindenevők! A sorrendet tartsuk.
   *
   * NOTE: egy megoldás lehet groupbyolni is, majd az eredményből ilyen formájú hármast készíteni
   */
  def sortAnimals(animals: Vector[Allat]) : (Vector[Allat],Vector[Allat],Vector[Allat]) = ???

  /**
   * FELADAT. Írjunk föggvényt, ami egy Vector[Eloleny]-ből készít egy (Vector[Allat],Vector[Noveny],Vector[Object]) hármast,
   * az első komponensbe az Allatokat, a másodikba a Novenyeket, a harmadikba azokat, melyek egyik traitet se extendelik,
   * teszi bele!
   *
   * NOTE: elviekben semmi nem tiltja, hogy valami egyszerre extendelje az Allat és a Noveny traitet.
   * A helyes tesztesethez ezt is kezelni kell és az ilyeneket berakni mindkét vektorba.
   */
  def sortBeings(beings: Vector[Eloleny]): (Vector[Allat], Vector[Noveny], Vector[Object] ) = ???

  /**
   * FELADAT. Készítsünk függvényt, mely paraméterként megkap egy nemnegatív egész összeget, és visszaad Map[Int,Int]-ként
   * egy címletezést: a map K kulcsához tartozó V érték azt adja vissza, hogy hány darabot használunk a K címletű pénbzől.
   * Egy olyan pénzváltást adjunk meg, mely a lehető legkevesebb darab pénzt használja a váltáshoz.
   * A magyar címletek legyenek a 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5.
   * A magyar szokásoknak megfelelően, az összeget kerekítsük a legközelebbi 5-tel oszthatóra!
   * Így például ha az input 1234, akkor az output m Mapre m(1000)=1, m(200)=1, m(20)=1, m(10)=1 és m(5)=1,
   * minden más c címletre m(c)=0 érkezzen vissza, és minden más x értékre, mely nem érvényes magyar címlet,
   * dobjon egy NoSuchElementExceptiont! tehát az előző esetre m(10000)=0 és m(3) dobja a kivételt.
   * Másik példa: 442-re az eredmény m-ben m(200)=2, m(20)=2 és minden más valid címletre 0.
   *
   * NOTE: érdemes lehet egy általánosabb függvényt írni, ami egy további paraméterben megkapja a lehetséges címleteket.
   */
  def magyarPenzValt(n: Int): Map[Int,Int] = ???

  /**
   * A múzeumokban műkincsek vannak, melyeknek van súlyuk és értékük.
   */
  case class Mukincs(weight: Int, price: Double)
  /**
   * FELADAT. Készítsünk függvényt, mely paraméterben megkap egy műkincs vektort és egy nemnegatív Int kapacitást,
   * és visszad egy olyan műkincs vektort, mely az inputként kapott műkincseknek egy olyan részhalmazát tartalmazza
   * (az inputban megadott eredeti sorrendben), melyeknek az összsúlyuk legfeljebb annyi, mint a megadott kapacitás,
   * ezen belül az összértékük pedig maximális!
   *
   * NOTE: érdemes lehet egy olyan belső függvényt írni, mely egy Map-ben kiszámolja az input műkincsvektor egy
   * prefixére, hogy ha k kapacitásunk van, akkor melyik tárgyat érdemes elvinni, és úgy mennyi lesz az összérték.
   * NOTE: this one is VERY hard.
   */
  def mukincsRablas(muzeum: Vector[Mukincs], ifaKapacitas: Int): Vector[Mukincs] = ???
}

package task05

import java.io.{BufferedReader, InputStreamReader}
import java.net.URL
import java.text.NumberFormat

object Anagrams {
  def time[U](n: Int)(block: =>U): U = {
    def sumTime(n: Int, acc: Long): Long = n match {
      case 0 => acc
      case _ => {
        val t0 = System.nanoTime()
        val result = block
        val t1 = System.nanoTime()
        sumTime(n-1, acc+(t1-t0))
      }
    }
    val totalTime = sumTime(n,0)
    val averageTime = totalTime / n
    println("Elapsed time: " + NumberFormat.getInstance().format(averageTime) + " ns, averaged over " + n + " calls")
    block
  }
  def timeOnce[U](block: =>U) = time(1)(block)

  val url = new URL("http://www.inf.u-szeged.hu/~szabivan/download/scala/words.txt")

  /**
   * Olvassuk be egy Vector[String]-be az in BufferedReader szöveges forrás sorai közül a nemüreseket
   * lowercaselve!
   * ehhez metódusok:
   * in.readLine() -- beolvassa a következő sort. Ha nincs több, akkor nullt ad, egyébként a sort mint String-et.
   */
  def readContents(in: BufferedReader): Vector[String] = {
    def readContents(acc: Vector[String]): Vector[String] = {
      val line = in.readLine
      line match {
        case null => acc
        case "" => readContents( acc )
        case _ => readContents( acc :+ line.toLowerCase )
      }
    }
    readContents( Vector() )
  }

  /**
   * Inicializáljuk a szavaink listáját:
   * - nyitjuk az URL-t, készítünk belőle InputStreamReadert, abból BufferedReadert,
   * - felolvassuk a sorait a vektorba --> csak ezt kell implementáljuk
   * - bezárjuk a readert
   * - visszaadjuk a stringvektort.
   * (Normális, Java filekezelésért, elvárható hibakezeléssel lásd majd a progegy gyakanyagot.)
   */
  val allWords: Vector[String] = {
    val in = new BufferedReader(new InputStreamReader(url.openStream()))
    val ret = readContents(in)
    in.close()
    ret
  }
  def stringToMap(s: String): Map[Char,Int] =
  s.foldLeft[Map[Char,Int]]( Map[Char,Int]().withDefaultValue(0)) {
    (acc, c) => acc.updated(c, acc(c)+1)
  }

  def areAnagrams(s1: String, s2: String): Boolean =
    stringToMap(s1) == stringToMap(s2)
  /**
   * A warm-up task: az inputként kapott s string összes *egyszavas* anagrammáját,
   * ami az allWords listában szerepel, készítsük el!
   * Az eredményt pakoljuk bele a Java oldalról kapott list-be.
   */
  def oneWordAnagrams(s: String, list: java.util.List[String]): Unit = {
    allWords.foreach(
      word => if (areAnagrams(word,s)) list.add(word)
    )
  }

  /**
   * Készítsünk el egy (String,String) listát, amiben az allWords-beli összes anagramma-pár szerepel!
   * Mondjuk a pár első komponense legyen az ábécé szerinti kisebb szó.
   * sanity check: 93 ilyen pár kéne legyen az outputban.
   */
  def anagramsInAllWords(list: java.util.List[(String,String)]): Unit =
    allWords.groupBy( _.sorted ).foreach(
      entry => { //entry: key: String, value: Vector[String]
        for(word1 <- entry._2; word2 <- entry._2) {
          if(word1<word2) list.add((word1,word2))
        }
      }
    )

  def subWord(word: String, input: String): Boolean =
    word.forall(
      ch => word.count( _ == ch ) <= input.count( _ == ch )
    )
  def removeWord(word: String, input: String): String = word match {
    case "" => input
    case _ => {
      val i = input.indexOf(word(0))
      val removed = input.substring(0,i) + input.drop(i+1) //kivesszük az i-edik karaktert, nem hatékonyan
      removeWord(word.drop(1), removed)
    }
  }

  def anagramsGenerator(input: String): Vector[Vector[String]] = input match {
    case "" => Vector(Vector())
    case _  => allWords.flatMap(
      word => {
        if( subWord(word,input) ) {
          val newInput = removeWord(word, input)
          anagramsGenerator(newInput).map( _ :+ word )
        } else Vector()
      }
    );
  }

  /**
   * A main task: az inputként kapott s string összes anagrammáját, ami előáll
   * az allWords lista szavaiból, mindegyiket csak egyszer, készítsük el!
   * Az eredményt pakoljuk bele a Java oldalról kapott list-be.
   * erdastuho => List("dare shout ", "read south ", "dare south ", ...)
   */
  def anagrams(s: String, list: java.util.List[String]): Unit = time(20) {  //time(20): mérje 20-szor le az időt és írja ki az átlagot
    //rekurzió: végigmegyünk az allWords-ön, ha egy word szó betűi <= s-é,
    //akkor kivesszük ezeket a betűket, rekurzívan hívjuk az anagramst,
    //aztán az eredmény lista elemei után még a word-öt is beszúrjuk.
    val allVector = anagramsGenerator(s)
    val allVectorSorted: Vector[String] = allVector.map { _.sorted }.distinct.map{ _.mkString(" ")}
    list.clear()
    allVectorSorted.foreach {
      vec => list.add( vec )
    }
  }
}

package task01

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class OpTojasTeszt extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  "Tarka tojás teszt" should "random inputra" in {
    for( _ <- 1 to 1000 ) {
      val input = (1 to 10) map { _ =>
        if( Random.nextInt() % 5 == 0 ) None
        else Some(Tojas(Random.nextInt(10), Random.nextInt(10).toBinaryString, Random.nextInt(10))) }
      val result = OpTojas.legTarkabbTojas( input.toVector )
      result match {
        case None => {
          for( i <- 20 to 0 by -1 if input.flatten.count ( _.szinekSzama >= i ) == 1 ) assert( false , s"hibás válasz: $input")
        }
        case Some(tojas) => {
          if ( (!(input.flatten contains tojas)) || input.flatten.count( t => t.szinekSzama >= tojas.szinekSzama ) != 1 ) assert( false , s"Hibás válasz: $input" )
          else ()
        }
      }
    }
  }
}

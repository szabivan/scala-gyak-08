package task04

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class PracticeMOVTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "Noveny" should "not be able to eat anything" in {
    val noveny = new Noveny {}  //NOTE: traitet így is lehet példányosítani, ha nincs benne absztrakt metódus
    assert( noveny.canEat( new Consumable {} ) == false )
  }
  "Ragadozo allat" should "eat other animals" in {
    //NOTE: traitet így is lehet példányosítani, ha van benne absztrakt metódus: kifejtjük őket
    val ragadozo = new Allat {
      override def isCarnivore: Boolean = true
      override def isHerbivore: Boolean = false
      override def isOmnivore: Boolean = false
    }
    assert( ragadozo.canEat( ragadozo ) );
    assert( !ragadozo.canEat( new Noveny{} ))
  }
  "Növényevő állat" should "eat plants and only plants" in {
    val novenyevo = new Allat {
      override def isCarnivore: Boolean = false
      override def isHerbivore: Boolean = true
      override def isOmnivore: Boolean = false
    }
    assert( novenyevo.canEat( new Noveny{} ))
    assert( !novenyevo.canEat(novenyevo))
  }
  "Mindenevő állat" should "mindent enni" in {
    val mindenevo = new Allat {
      override def isCarnivore: Boolean = false
      override def isHerbivore: Boolean = false
      override def isOmnivore: Boolean = true
    }
    assert(mindenevo.canEat(mindenevo))
    assert(mindenevo.canEat(new Noveny{}))
  }

  //tesztekhez készítünk néhány case objectet
  case object Eper extends Noveny
  case object Szilva extends Noveny
  case object Cannabis extends Noveny
  trait Novenyevo extends Allat { //specializálhatjuk a traitet további traitté és benne overrideolhatunk default implementációkat
    override def isCarnivore = false
    override def isHerbivore = true
    override def isOmnivore = false
  }
  case object Tehen extends Novenyevo //így már nincs benne absztrakt metódus
  case object Lo extends Novenyevo
  trait Ragadozo extends Allat { //specializálhatjuk a traitet további traitté és benne overrideolhatunk default implementációkat
    override def isCarnivore = true
    override def isHerbivore = false
    override def isOmnivore = false
  }
  case object Kutya extends Ragadozo
  case object Macska extends Ragadozo
  trait Mindenevo extends Allat {
    override def isCarnivore = false
    override def isHerbivore = false
    override def isOmnivore = true
  }
  case object Majom extends Mindenevo

  case object Gomba extends Eloleny { //nem állat, nem növény, nem eszik semmit
    override def canEat(food: Consumable): Boolean = false
  }
  val beings = Vector(Eper, Szilva, Cannabis, Tehen, Lo, Kutya, Macska, Majom, Gomba)
  val animals = beings collect { case animal : Allat => animal } //Vector[Allat](Tehen, Lo, Kutya, Macska, Majom), ld lentebb

  //tesztek és sample implementációk
  def castToAllat(being: Eloleny): Option[Allat] = being match {
    case allat: Allat => Some(allat)
    case _ => None
  }
  "castToAllat" should "cast exactly those Elolenysz to Allat options which are actually Allatsz" in {
    for( being <- beings ){
      assert( castToAllat(being) == PracticeMOV.castToAllat(being), "teszteset: " + being )
    }
  }
  //NOTE: a  flatMap castToNoveny is egy jó megoldás; a collect metódussal  meg lehet oldani, worth to check it.
  //a collect metódus kap egy *parciális függvényt* (a match kifejezés tulajdonképpen annyit tesz, hogy kiértékeli
  // a mövötte álló parciális függvényt és ha nincs értéke, akkor dob egy MatchError-t; a c.collect(f) nem dob match
  // errort, hanem ami előáll függvényértékként, azt beteszi az eredménybe, ahol meg nem értelmezett a függvény,
  // azt csak átlépi.
  def collectPlants(beings: Vector[Eloleny]): Vector[Noveny] =  beings collect { case noveny: Noveny => noveny }
  "collectPlants" should "collect all the Plants from the input" in {
    assert( PracticeMOV.collectPlants(beings) == collectPlants(beings))
  }

  def sortAnimals(animals: Vector[Allat]) : (Vector[Allat],Vector[Allat],Vector[Allat]) =
    (animals.filter( _.isCarnivore), animals.filter( _.isHerbivore), animals.filter(_.isOmnivore))
  // úgy is jó, ha a groupBy( x => if( x.isCarnivore ) 0 else if( x.isHerbivore ) 1 else if( x.isOmnivore ) 2 else 3 groupbyból rakjuk össze
  "sortAnimals" should "cluster the animals based on their diets" in {
    assert( PracticeMOV.sortAnimals(animals) == sortAnimals(animals))
  }

  def sortBeings(beings: Vector[Eloleny]): (Vector[Allat], Vector[Noveny], Vector[Object] ) = (
    beings collect { case allat: Allat => allat },
    beings collect { case noveny: Noveny => noveny },
    beings flatMap { x => x match { case _: Allat => None; case _: Noveny => None; case _ => Some(x) }} //opcióba flatmapelni is OK
  )
  "sortBeings" should "cluster the beings based on whether they are animals, plants or none" in {
    assert( PracticeMOV.sortBeings(beings) == sortBeings(beings) )
  }

  def greedyPenzValt(n: Int, denominations: Vector[Int], mapSoFar: Map[Int,Int]):Map[Int,Int] = {
    if(denominations.isEmpty) mapSoFar
    else greedyPenzValt(n % denominations(0), denominations.drop(1), mapSoFar.updated( denominations(0), n / denominations(0) ) )
  }
  def magyarPenzValt(n: Int): Map[Int,Int] = greedyPenzValt(((n+2)/5)*5, Vector(20000,10000,5000,2000,1000,500,200,100,50,20,10,5), Map())
  "magyarPenzvalt" should "change the sum, rounded to fives, into valid hungarian denominations, using the least coins/banknotes possible" in {
    for( i <- 1 to 1000 ){
      val n = Random.nextInt(50000);
      assert(magyarPenzValt(n) == PracticeMOV.magyarPenzValt(n))
    }
  }

  import PracticeMOV.Mukincs
  def mukincsRablas(muzeum: Vector[Mukincs], ifaKapacitas: Int): Vector[Mukincs] = {
    def mukincsTable(n: Int, soFar: Vector[Map[Int,(Double,Boolean)]]): Vector[Map[Int, (Double,Boolean)]] = {
      if( n > muzeum.length ) soFar
      else {
        val prevMap = soFar(n-1)
        val currentMukincs = muzeum(n-1)
        val newMap = (ifaKapacitas to currentMukincs.weight by -1 ).foldLeft[Map[Int, (Double,Boolean)]](Map().withDefaultValue(0.0,false)){
          (currentMap,kapacitas) => {
            val smolOpt: (Double,Boolean) = prevMap(kapacitas - currentMukincs.weight)
            if( smolOpt._1 + currentMukincs.price > prevMap(kapacitas)._1 )
              currentMap.updated(kapacitas, (smolOpt._1 + currentMukincs.price, true))
            else currentMap.updated(kapacitas, (prevMap(kapacitas)._1,false))
          }
        }
        mukincsTable(n+1, soFar :+ newMap )
      }
    }
    val table = mukincsTable(1,Vector(Map().withDefaultValue(0.0,false)))
    def decodeTable(n: Int, capacity: Int, acc: Vector[Mukincs]): Vector[Mukincs] = {
      if( n == 0 ) acc
      else table(n)(capacity) match {
        case (_,false) => decodeTable(n-1, capacity, acc)
        case (_,true) => decodeTable(n-1, capacity-muzeum(n-1).weight, muzeum(n-1) +: acc )
      }
    }
    decodeTable(muzeum.length, ifaKapacitas, Vector())
  }
  def generateRandomMukincssz: Vector[Mukincs] = Random.nextInt(30) match {
    case 0 => Vector()
    case _ => generateRandomMukincssz :+ Mukincs(Random.nextInt(10)+1, Math.round(Random.nextDouble()*100.0))
  }

  "mukincsRablas" should "steal artifacts maximizing their total price" in {
    for( i <- 1 to 100 ) {
      val muzeum = generateRandomMukincssz
      val ifa = Random.nextInt(50)+50
      val optVector = mukincsRablas(muzeum, ifa)
      val localVector = PracticeMOV.mukincsRablas(muzeum, ifa)
      assert(optVector == localVector,"input: " + muzeum + " kapacitas: " + ifa + " opt: " + optVector + " wrong answer: " + localVector)
    }
  }


}

package task02

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import VectorUtils._

import scala.util.Random

class HashTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "getIndexFrom" should "work for empty vector and for over/underindices" in {
    assert( getIndexFrom[String](Vector(),"sanyi",0) == None )
    assert( getIndexFrom[String](Vector(),"sanyi",-1) == None )
    assert( getIndexFrom[String](Vector(),"sanyi",1) == None )
  }

  "getIndexFrom" should "return the Option with the index" in {
    for(k<-1 to 1000) {
      val e = Random.nextInt(20)
      val i = Random.nextInt(10)
      def generateVector(acc: Vector[Int], accRet: Option[Int]): (Vector[Int],Option[Int]) = {
        Random.nextInt(20) match {
          case 0 => (acc,accRet)
          case _ => {
            val next = Random.nextInt(20)
            if ((accRet==None)&&(next == e)&&(acc.length>=i)) generateVector(acc:+next,Some(acc.length))
            else generateVector(acc:+next,accRet)
          }
        }
      }
      val (testVec, testResult) = generateVector(Vector(),None)
      assert( getIndexFrom(testVec, e, i) == testResult , "teszteszet: " + testVec + ", e=" + e+ ", i="+ i)
    }
  }

  "getIndex" should "return the Option with the first index" in {
    for(k<-1 to 1000) {
      val e = Random.nextInt(20)
      def generateVector(acc: Vector[Int], accRet: Option[Int]): (Vector[Int],Option[Int]) = {
        Random.nextInt(20) match {
          case 0 => (acc,accRet)
          case _ => {
            val next = Random.nextInt(20)
            if ((accRet==None)&&(next == e)) generateVector(acc:+next,Some(acc.length))
            else generateVector(acc:+next,accRet)
          }
        }
      }
      val (testVec, testResult) = generateVector(Vector(),None)
      assert( getIndex(testVec, e) == testResult , "teszteszet: " + testVec + ", e=" + e)
    }
  }

  "getIndex" should "work for empty vector" in {
    assert( getIndex[String](Vector(),"sanyi") == None )
  }

  "removeSwap" should "not touch the vector if the element sis not present" in { for( k <- 1 to 1000 ) {
    def generateVector: Vector[Int] = Random.nextInt(20) match {
      case 0 => Vector()
      case _ => generateVector :+ Random.nextInt(20)
    }
    val vector = generateVector
    def generateNotElement: Int = {
      val e = Random.nextInt(25)
      if( vector contains e ) generateNotElement else e
    }
    val e = generateNotElement
    assert( removeSwap(vector,e) == vector, "teszteset: " + vector + ", e = " + e)
  }}

  "removeSwap" should "remove the FIRST element by merging in the last one" in {
    for (k <- 1 to 1000) {
      def generateVector: Vector[Int] = Random.nextInt(20) match {
        case 0 => Vector()
        case _ => generateVector :+ Random.nextInt(20)
      }
      val e = Random.nextInt(10)+30
      val testVectorPrefix = generateVector
      val testVectorMidix = generateVector
      val testVectorSuffix = generateVector
      val testVectorLast = Random.nextInt(10)
      val testVector = testVectorPrefix :+ e :+ testVectorMidix :+ e :+ testVectorSuffix :+ testVectorLast
      assert(removeSwap( testVector , e)
      == testVectorPrefix :+ testVectorLast :+ testVectorMidix :+ e :+ testVectorSuffix,
      "teszteset: " + testVector + ", e = " + e)
    }
  }

  "HashHalmaz[String]()" should "return a HashHalmaz(Vector(Vector(),Vector(),Vector(),Vector(),Vector(),Vector(),Vector(),Vector()), 8)" in {
    assert(HashHalmaz[String]() ==
    HashHalmaz[String](Vector(Vector(),Vector(),Vector(),Vector(),Vector(),Vector(),Vector(),Vector()),8))
  }

  "HashHalmaz.loadFactor" should "return the RATIO of nonempty vectors" in {
    def generateVector: Vector[Int] = Random.nextInt(20) match {
      case 0 => Vector()
      case _ => generateVector :+ Random.nextInt(20)
    }
    def generateVectors: (Vector[Vector[Int]],Double) = Random.nextInt(20) match {
      case 0 => (Vector(Vector()),0)
      case _ => {
        val (pre,so) = generateVectors
        val next = generateVector
        (pre :+ next, if(next.isEmpty) so else (so+1))
      }
    }
    for (k<-1 to 1000) {
      val (test,no) = generateVectors
      assert( HashHalmaz(test,test.length).loadFactor == no/test.length, "teszteset: " + HashHalmaz(test,test.length) )
    }
  }

  "offset" should "return the mod-class of e.hashCode modulo size" in {
    for( mod <- 2 to 10 ; result <- 0 until mod; value <- -5 to 5 ) {
      assert(HashHalmaz(Vector(),mod).offset(new Object(){ override def hashCode = result + value*mod }) == result,
      "teszteset: size = " + mod + ", hashCode = " + value )
    }
  }

  case class ModHash(n: Int){ override def hashCode = n }

  "contains" should "find an element which is present" in {
    val hash = HashHalmaz(
      Vector(
        Vector(ModHash(0), ModHash(6), ModHash(9)),
        Vector(),
        Vector(ModHash(2), ModHash(-1))
      ) , 3)
    for( n <- Vector(0,6,9,2,-1)) assert( hash.contains(ModHash(n)), "teszteset: " + hash + " contains ModHash(" + n + ")")
  }

  "contains" should "not find an element which is present" in {
    val hash = HashHalmaz(
      Vector(
        Vector(ModHash(0), ModHash(6), ModHash(9)),
        Vector(),
        Vector(ModHash(2), ModHash(-1))
      ) , 3)
    for( n <- Vector(1,3,-2,4)) assert( !hash.contains(ModHash(n)), "teszteset: " + hash + " contains ModHash(" + n + ")")
  }

  "insert" should "insert a couple of elements according to their hashCodes" in {
    val result = Vector(2,0,6,-1,9).foldLeft(HashHalmaz[ModHash](Vector(Vector(),Vector(),Vector()),3)){
      (hh,v) => hh.insert(ModHash(v))
    }
    assert( result ==
      HashHalmaz(Vector(
        Vector(ModHash(0), ModHash(6), ModHash(9)),
        Vector(),
        Vector(ModHash(2), ModHash(-1))
      ),3) , "inserting the elements 2,0,6,-1,9 into a HashHalmaz of size = 3 we got " + result)
  }

  "insert" should "not insert repetitions" in {
    val result = Vector(2,0,6,0,2,-1,9,-1).foldLeft(HashHalmaz[ModHash](Vector(Vector(),Vector(),Vector()),3)){
      (hh,v) => hh.insert(ModHash(v))
    }
    assert( result ==
      HashHalmaz(Vector(
        Vector(ModHash(0), ModHash(6), ModHash(9)),
        Vector(),
        Vector(ModHash(2), ModHash(-1))
      ),3) , "inserting the elements 2,0,6,0,2,-1,9,-1 into a HashHalmaz of size = 3 we got " + result)
  }

  "insert" should "resize the hashhalmaz if it becomes too densely populated" in {
    val input = HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6), ModHash(9)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3)
    val result = input.insert(ModHash(1))
    assert( result == HashHalmaz(Vector(
      Vector(ModHash(0)),
      Vector(ModHash(1)),
      Vector(ModHash(2)),
      Vector(), Vector(), Vector(),
      Vector(ModHash(6)),
      Vector(), Vector(),
      Vector(ModHash(9)),
      Vector(),
      Vector(ModHash(-1))
    ),12), "teszteset: " + input + " insert 1" )
  }

  "resizeIfNeeded" should "not touch sparse hashsets" in {
    val testHH = HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6), ModHash(9)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3)
    assert(testHH.resizeIfNeeded == testHH, "teszteset: " + testHH)
  }

  "resizeIfNeeded" should "double densely populated hashsets" in {
    val testHH = HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6), ModHash(9)),
      Vector(ModHash(1)),
      Vector(ModHash(2), ModHash(-1))
    ),3)
    assert( testHH.resizeIfNeeded
      ==
      HashHalmaz(Vector(
        Vector(ModHash(0)),
        Vector(ModHash(1)),
        Vector(ModHash(2)),
        Vector(), Vector(), Vector(),
        Vector(ModHash(6)),
        Vector(), Vector(),
        Vector(ModHash(9)),
        Vector(),
        Vector(ModHash(-1))
      ),12), "teszteset: " + testHH)
  }

  "remove" should "not touch the hashhalmaz if the element is not present" in {
    val testHH = HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6), ModHash(9)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3)
    for(k <- Vector(1,8,7,-2,10)) assert( testHH.remove(ModHash(k))==testHH, "teszteset: " + testHH + " remove " + k)
  }

  "remove" should "remove elements from the hashhalmaz" in {
    val testHH = HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6), ModHash(9)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3)
    assert(testHH.remove(ModHash(9)) ==HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3) , "teszteset: " + testHH + " remove 9 ")
    assert(testHH.remove(ModHash(-1)) ==HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6), ModHash(9)),
      Vector(),
      Vector(ModHash(2))
    ),3) , "teszteset: " + testHH + " remove (-1) ")
  }

  "remove" should "remove elements from the hashhalmaz, correctly" in {
    val testHH = HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6), ModHash(9)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3)
    assert(testHH.remove(ModHash(6)) ==HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(9)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3) , "teszteset: " + testHH + " remove 6 ")
    assert(testHH.remove(ModHash(0)) ==HashHalmaz(Vector(
      Vector(ModHash(9), ModHash(6)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3) , "teszteset: " + testHH + " remove 0 ")
    assert(testHH.remove(ModHash(2)) ==HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6),ModHash(9)),
      Vector(),
      Vector(ModHash(-1))
    ),3) , "teszteset: " + testHH + " remove 2 ")
    assert(testHH.remove(ModHash(2)).remove(ModHash(-1)) ==HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6),ModHash(9)),
      Vector(),
      Vector()
    ),3) , "teszteset: " + testHH + " remove 2 remove (-1)")
  }

  "foreach" should "be applied to each element of the hashhalmaz" in {
    val testHH = HashHalmaz(Vector(
      Vector(ModHash(0), ModHash(6), ModHash(9)),
      Vector(),
      Vector(ModHash(2), ModHash(-1))
    ),3)
    var s: String = ""
    testHH foreach { x => s += x.n }
    assert( s == "0692-1")
  }

}
